## Refonte graphique du site "mi-aime-a-ou.com"

///

Pour rappel, les consignes particulières du projet (à privilégier à celles données sur Simplonline) :
* travail sur la page d'accueil uniquement
* utiliser Flexbox
* durée du projet : le reste de la journée + demain éventuellement (organisation libre pour vous avancer sur ce dont vous avez besoin)
* Dépôt GitHub